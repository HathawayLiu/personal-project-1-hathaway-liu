+++
title = "About Me"
+++
<div style="display: flex; align-items: flex-start;">
    <!-- Profile Picture -->
    <div style="flex: 0 0 auto; margin-right: 40px;">
        <img src="/about/hathy.png" alt="Hathaway Liu" style="width: 250px;">
    </div>

<!-- Bio Information -->
<div style="flex: 1 1 auto;">
<h1>Hathaway(Xinyan) Liu</h1>
<h2>About</h2>
    <ul>
        Hi! My name is Xinyan Liu. You can call me Hathaway. I'm a current Master student in Statistical Science from Duke University with a solid foundation in Mathematics from the University of Washington. I have hands-on experience in designing data-driven recommendation systems, optimizing computational algorithms, and leading extensive projects ranging from a COVID-19 Vaccine Reservation System to environmental performance analyses. My diverse skills encompass Java, Python, R, SQL, and more, bolstered by my research publication accepted by ICBAR2022, relevant courseworks, and past project experiences. Additionally, my leadership roles in various organizations have refined my team collaboration and strategic planning abilities. With a knack for extracting insights from complex datasets and a passion for pushing the boundaries of data analysis, I'm currently seeking a Data Analyst/Scientist intern position to further harness my skills and contribute to data-driven solutions.
    </ul>
<h2>Contact Information</h2>
    <ul>
        <li>Email: <a href="mailto:hathaway.liu@duke.edu">hathaway.liu@duke.edu</a></li>
        <li>LinkedIn: <a href="https://www.linkedin.com/in/hathaway-liu/">Hathaway Liu</a></li>
    </ul>

<h2>Education</h2>
    <ul>
        <li>Graduate school: <b>Duke University</b>, Master in <b>Statistical Science</b></li>
        <li>Undergraduate school: <b>University of Washington</b>, Seattle, Bachelors of Science in <b>Mathematics</b></li>
    </ul>

<h2>Experience</h2>
    <ul>
        <li>Current <b>Data Analyst Intern</b> in <b>Baidu</b></li>
        <li>Former <b>Marketing Intern</b> in <b>EasyTransfer</b></li>
    </ul>

<h2>Publications</h2>
        <ul>
            <li>Independent author, <b>"Fast Recommender System Combining Global and Local Information"</b>, accepted by 2022 2nd International Conference on Big Data, Artificial Intelligence and Risk Management (ICBAR2022)</li>
        </ul>
    </div>
</div>
