+++
title = "Mini Project 1: Static website with Zola"
description = "Create a static website with Zola and styled it with CSS for future use"
date = 2024-01-28
template = "non-post-page.html"

[extra]
toc = true
+++
[**Project Gitlab Repo**](https://gitlab.com/HathawayLiu/personal-website)

Welcome to this guide on creating a static website with Zola. This project is designed to help you get started with Zola, a fast static site generator in a single binary with everything built-in. By the end of this project, you'll have a sleek, styled static website ready for your content.

## Getting Started with Zola

1. **Installation:** First, you need to install Zola. Visit the [official Zola documentation](https://www.getzola.org/documentation/getting-started/installation/) for detailed installation instructions for your operating system.

2. **Creating a New Site:** Once Zola is installed, you can create a new site by running the following command in your terminal:

    ```shell
    zola init my_site
    ```

    Replace `my_site` with the name of your project. This command creates a new directory with some basic configuration files and folders.

3. **Exploring the Directory Structure:** Zola's initial directory structure includes several key folders:

    - `content`: Where your markdown files will live.
    - `static`: For static files like images and CSS that don't change.
    - `templates`: Contains HTML templates for your site.
    - `themes`: Contains HTML themes for your site.

4. **Adding Content:** Create a new markdown file in the `content` directory to start adding content to your site. You can use the header format provided at the beginning of this document as a template for your content files.

5. **Using themes:** If you don't want to create your own themes, there are plenty of themes that you could use in the Zola website. Navigate to the [Themes in Zola](https://www.getzola.org/themes/), and simply type the following command in your own terminal:

```shell
cd themes
git clone < theme repository URL >
```
You can go under any theme post and find the corresponding repo to replace. Now, you can go to the theme directory and change the `Config.toml` with your own choice.

## Styling Your Site with CSS

To style your site, create a CSS file in the `static` directory. Here's a simple CSS file to get you started:

```css
/* static/style.css */
body {
    font-family: Arial, sans-serif;
    margin: 0;
    padding: 0;
    background-color: #f4f4f4;
}

h1, h2, h3, h4, h5, h6 {
    color: #333;
}

p {
    color: #666;
}
```

Link this CSS file in your HTML template. For example, if you're using the non-post-page.html template, add the following line in the `<head>` section:

```html
<link rel="stylesheet" href="/style.css">
```

## Test and deploy your website

With Zola, you can first test your website by running `zola serve`. A url will appear in the terminal and you can get a preview of how your wesite looks. Then, you can build your site by running `zola build`. This command generates a public directory with your site's static files, ready to be deployed to a web server.