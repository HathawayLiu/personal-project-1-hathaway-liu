+++
title = "Mini Project 5: Serverless Rust Microservice"
description = "reate a Rust AWS Lambda function (or app runner) and connect it to a database"
date = 2024-02-26
template = "non-post-page.html"

[extra]
toc = true
+++
[**Project Gitlab Repo**](https://gitlab.com/HathawayLiu/mini_proj_5_hathaway_liu)
## Project Description

This project requries us to create a Rust AWS Lambda function (or app runner) like what we did in Mini Project 2, then implement a simple service and connect to a database of our own choice. For this project, I choose to write a Rust Lambda function of simulating dice rolling and connect to the `AWS DynamoDB`.

## Table Structure
The Rust Lambda function creates a table named `dice_rolls` and connect to `DynamoDB` with the same name. The table contains two columns:
- `roll_id`: String
- `roll_value`: Interger

## Get started with Cargo Lambda
1. You can refer to the official [Cargo Lambda website](https://www.cargo-lambda.info/guide/getting-started.html) to get your hands on installing Cargo lambda on your own device.
2. Then you can run `cargo lambda new <Your own project name>` to create your own project. Make sure that you replace your own project name inside.
3. Go to `main.rs` and start to write your own lambda function. Make sure to add the corresponding dependencies in `Cargo.toml`.
4. After writing your contend, you can use the following command to test your function:
```shell
cargo fmt --quiet
cargo clippy --quiet
cargo test --quiet
```
5. Without any error, you can run `cargo lambda watch` in one terminal; then open a new terminal and use `cargo lambda invoke --data-ascii "{ \"command\": \"encrypt\", \"message\": \"encrypt\" }"` to test if your function is working. Then you can run `cargo lambda build --release --arm64` to build your function.

## Detail Steps
### Preparation Steps
1. Like what we did in other projects, create a user under `AWS IAM`. Add permissions to the following policies: `iamfullaccess`, `lambdafullaccess`, `AmazonDynamoDBFullAccess`. Then, under Secruity Credentials, generate a new `Access Key` and store it in somewhere save.
2. Go to `DynamoDB`, create a table with your own choice of name and partition key(i.e. primary key). 
3. Go to your own terminal, use `cargo lambda new <Your Project Name>` to create a new project 

### Implementation Steps
1. Go to your `main.rs` and starts to write your own function. When you are writing code to connect to `DynamoDB`, make sure that your table name and key name match the one you already created on `DynamoDB`.
2. Use necessary header and add the corresponding dependencies in `Cargo.toml`. You can refer to mine for help.
3. Add a corresponding `Makefile`. You can download mine and use it. Make sure to change the `invoke` command to match your own function.
4. Now, run `cargo lambda watch` to allow the function running. Open another terminal, run `cargo lambda invoke --data-ascii "{ \"command\": \"encrypt\", \"message\": \"encrypt\" }"` to test if your function gives the correct output. 
5. Create a `.env` file and add your two access keys and access region to the file with the following structure:
```plaintext
AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_REGION=
```
Then create a `.gitignore` file:
```plaintext
/target
.env
```
Then use either `Export` or `set -a` and `source .env` to allow your terminal to detect your AWS user

6. Then run `cargo lambda build` and `cargo lambda deploy` to deploy your lambda function.
7. Go to `Lambda` in AWS Console, check if your function is already on there. Then go to `Configuration` and click on your `Role name`. It will lead you to a page and you can add permission to `AmazonDynamoDBFullAccess` policies just like what you did before. 
8. Go to `API Gateway`, create a `REST API` with the stage you create. Then create a method under it. Make sure you select type `ANY`. Finally you can deploy your API and you will be able to see a API like the following: https://nor4ymmx4e.execute-api.us-east-1.amazonaws.com/Proj_5/ .
9. You can test your API using following command:
```
curl -X POST https://nor4ymmx4e.execute-api.us-east-1.amazonaws.com/Proj_5/ \
  -H 'content-type: application/json' \
  -d '{ "command": "roll"}'
```
make sure to replace it with your own API and own command.

## Deliverables
- DynamoDB Table
![table](https://gitlab.com/HathawayLiu/mini_proj_5_hathaway_liu/-/wikis/uploads/ccdc942135dcbb8a798abece96c5e6d5/Screenshot_2024-02-26_at_3.44.35_PM.png)
![items](https://gitlab.com/HathawayLiu/mini_proj_5_hathaway_liu/-/wikis/uploads/563968acc032b41f3a81bd0708ae33a3/Screenshot_2024-02-26_at_3.44.42_PM.png)
- Lambda function
![lambda](https://gitlab.com/HathawayLiu/mini_proj_5_hathaway_liu/-/wikis/uploads/3a04bb8ae70adb0e5b3c7aa8d38c70d3/Screenshot_2024-02-26_at_3.46.51_PM.png)
