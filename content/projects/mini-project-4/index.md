+++
title = "Mini Project 4: Rust Arctix Web App"
description = "Create a simple Arctix Web App with Rust"
date = 2024-02-21
template = "non-post-page.html"

[extra]
toc = true
+++
[**Project Gitlab Repo**](https://gitlab.com/HathawayLiu/mini_proj_4_hathaway_liu)
## Project Description
This is a project that uses `Rust` to create a simple Arctix Web App. Typically, it requires to containerize simple Rust Actix web app, build a docker image, and run container locally. To run the container locally, it is required to download `Docker Desktop`.

## Function details
For this web app, I wrote a rust function in `main.rs` that creates a simple app to simulate dice rolling. When users click of the `Roll Dice` button, the web app will randomly generate a number from 1 to 6 and display on the website.

**Before you go into the details, please download `Docker Destop` first on your own device.**
## Detailed Steps
1. Open terminal, use `cargo new <YOUR PROJECT NAME>` to create a new rust project.
2. Go to `/src/main.rs`, write you own function to create your web app. Remember to include `use actix_web::{web, App, HttpResponse, HttpServer, Responder}` and `use actix_files::Files` on top of the file.
3. For a better UI design for the website, create a `static` folder and create a new `index.html` under this folder. Write necessary html code for CSS styling. Go back to your `main.rs`, include `.service(Files::new("/", "./static").index_file("index.html"))` under `App::new()` to make sure the styling apply to the web app.
4. In `Cargo.toml`, include the following dependencies (or more depends on your own app):
    - `actix-web`: web framework for Rust
    - `actix-files`: handle serving static files
    - `rand`: generating random numbers in Rust
5. Create a corresponding `Dockerfile` and `Makefile`. You can simply copy mine, but make sure that the name of project for `Dockerfile` and image name in `Makefile` match the `name` in `Cargo.toml`.
6. Make sure your `Docker Desktop` is open. Open a terminal, cd to your current directory, and run `cargo build` and `cargo run`. Open another terminal in the same time, run `docker build -t <YOUR IMAGE NAME> .` You should be able to see your web app under `http://localhost:8080/`
7. Finally run `docker run -d -p 8080:8080 <YOUR IMAGE NAME>`. Open `Docker Desktop`, under your `Container` you should be able to open the `Port`.

## Web App and Docker Container Screenshot
- Web App of `Dice Rolling` running successfully
![Web App 1](https://gitlab.com/HathawayLiu/mini_proj_4_hathaway_liu/-/wikis/uploads/5ef15a3b45aee31841c272e0fe8537e5/Screenshot_2024-02-20_at_9.07.33_PM.png)
![Web App 2](https://gitlab.com/HathawayLiu/mini_proj_4_hathaway_liu/-/wikis/uploads/e9ec1bffb08b872836f9d31df5a867bf/Screenshot_2024-02-20_at_9.07.41_PM.png)
- Docker Container
![Container](https://gitlab.com/HathawayLiu/mini_proj_4_hathaway_liu/-/wikis/uploads/4cd6400e59d0ca149c6ee7bd84fab1cf/Screenshot_2024-02-20_at_9.08.32_PM.png)
