+++
title = "Mini Project 3: S3 Bucket with AWS"
description = "Create a simple S3 Bucket with CDK app code"
date = 2024-02-13
template = "non-post-page.html"

[extra]
toc = true
+++
[**Project Gitlab Repo**](https://gitlab.com/dukeaiml/IDS721/mini-project-3)
## Project Description

This is a project that creates a S3 bucket using AWS CDK. It uses an environment created by `AWS CodeCatalyst` and generates CDK app code using `AWS Cloud 9`. Also, the CDE code is generated with the help of `CodeWhisperer` to create the properties of the versioning and encryption. Details will be described below

## Steps

1. Open [CodeCatalyst](https://codecatalyst.aws/explore) and sign in. Create a project by selecting `start from scratch` and then create a new environment under the new project. 
2. Select `AWS Cloud 9` to open the new dev environment.
3. Go to AWS IAM and create a new user.
4. Under the new user, add permissions to the following policies: `IAMFullAccess`, `AmazonS3FullAccess` and `AWSLambda_FullAccess`.
5. After this, click on `add permissions` to add the following inline policies: `Cloud Formation`, `Systems Manager`. Remember to select all access.
6. Go to `Security Credentials`, create access key under selected user and download the csv file to store the access key.
7. Go back to Cloud 9, type `aws configure` in terminal.
8. Type `mkdir YOUR_PROJECT_NAME` to create a new project directory. Then `cd` to the directory and type `cdk init app --language=typescript` to create the project template.
9. Ultilize CodeWhisperer to generate code for a S3 Bucket:
    * Under `\lib\mini-proj-3-stack.ts`, use the prompt `//make an S3 bucket that enables versioning and encrption` to allow CodeWhisperer to suggest code for making the appropriate S3 Bucket.
    * Under `\bin\mini-proj-3.ts`, use the prompt `//add necessary variables so that a S3 bucket is created and is deployed correctly` to add variables to make a S3 bucket.
10. After finishing with the code, use `npm run build` compile typescript to js.
11. Then use `cdk synth` emits the synthesized CloudFormation template.
12. Finaly, use `cdk deploy` deploy this stack to default AWS account/region.
13. Go to AWS and search `S3` to check if the bucket is deployed.

 ## Generated S3 bucket screenshot
 * S3 bucket overview
![](https://gitlab.com/dukeaiml/IDS721/mini-project-3/-/wikis/uploads/5e0f1c92183fe38b5af631f7ac1d4dc0/s3-bucket.png)
 * S3 bucket properties of versioning and encryption
![](https://gitlab.com/dukeaiml/IDS721/mini-project-3/-/wikis/uploads/2deac6e609aea5e5db2e2a08f3e7005f/encryption.png)
![](https://gitlab.com/dukeaiml/IDS721/mini-project-3/-/wikis/uploads/8811936b30f79f0344f2f0ca6cbcc2cc/version.png)