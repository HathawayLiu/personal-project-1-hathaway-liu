+++
title = "Mini Project 2: Cargo Lambda function with Rust"
description = "Create a simple Cargo Lambda function with Rust to do data transformation"
date = 2024-02-06
template = "non-post-page.html"

[extra]
toc = true
+++
[**Project Gitlab Repo**](https://gitlab.com/HathawayLiu/mini-project-2)
## Project description
This program is built with Cargo Lambda and Rust. This program designed for processing and aggregating data from [Palmer Penguin](https://allisonhorst.github.io/palmerpenguins/) dataset, where it reads the dataset in a CSV format, takes in a specified numerical value(float type) for filtering the data, and returns the mean of selected variables in the dataset by penguin species. It uses the `serde` crate for serialization and deserialization, suggesting that it handles JSON data. It also has the the ability to handle requests in a serverless environment.

## Get started with Cargo Lambda
1. You can refer to the official [Cargo Lambda website](https://www.cargo-lambda.info/guide/getting-started.html) to get your hands on installing Cargo lambda on your own device.
2. Then you can run `cargo lambda new <Your own project name>` to create your own project. Make sure that you replace your own project name inside.
3. Go to `main.rs` and start to write your own lambda function. Make sure to add the corresponding dependencies in `Cargo.toml`.
4. After writing your contend, you can use the following command to test your function:
```shell
cargo fmt --quiet
cargo clippy --quiet
cargo test --quiet
```
5. Without any error, you can run `cargo lambda watch` in one terminal; then open a new terminal and use `cargo lambda invoke --data-ascii "{ \"command\": \"encrypt\", \"message\": \"encrypt\" }"` to test if your function is working. Then you can run `cargo lambda build --release --arm64` to build your function.

## File Structure
```plaintext
transform/
├── src/                # Source files
│   ├── main.rs         # Main application file for cargo lambda pipeline
│   └── lib.rs          # Main data transformation function
├── Makefile            # short cut for watch, invoke and deploy
├── README.md           # Program overview and instructions
├── Cargo.toml          # Rust package manifest
├── Cargo.lock          
├── .gitlab-ci.yml
└── .gitignore          # Specifies intentionally untracked files to ignore
```
## Function Usage
This program reads the Penguins Dataset from `PENGUIN_DATA`. It assumes the CSV has headers.
The following describe detailed transformation of the dataset:
1. **Filtering**: It filters the rows where the column `culmen_depth_mm` is greater than some specified filter value that user passed in (i.e. 13.5).
2. **Grouping**: The program then groups the data by the `species` column.
3. **Aggregation**: For each group (species in this case), it calculates the mean of `culmen_length_mm`, `culmen_depth_mm`, `flipper_length_mm`.

**Output**: The output dataset will be a dataframe in shape (3, 4), where it is stored as a JSON object and contains mean of the variables mentioned above after filtering by specific value. An error message will be raised if invalid value is inputted. Below are the columns of resulted dataframe:
- `species`: The name of the penguin species
- `culmen_length_mean`: The mean of `culmen_length_mm` values for each species
- `culmen_depth_mean`: The mean of `culmen_depth_mm` values for each species
- `flipper_length_mean`: The mean of `flipper_length_mm` values for each species

## Deployment
### Deploy to AWS
The lambda function we wrote could be deployed to Amazon AWS. To achieve this, you could follow the following instructions:
1. Make sure that you have your own AWS account and sign into it. Go to `AWS IAM`, under User, create a new user and add permissions to the following polices: `iamfullaccess`, `lambdafullaccess`. (The above two policies are specifically for this project, you could modify by your own choice).
2. Go to `Secruity Credentials`, create a new Access Key for your new created user. Make sure to store those two keys in somewhere safe.
3. Go back to your lambda project directory, create a new `.env` file using the following template:
```plaintext
AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_REGION=
```
Replace your own key and region inside. And also create a `.gitignore` file by following:
```plaintext
/target
.env
```
so that your `.env` won't show up when you push your file to your own Gitlab. Then use either `Export` or `set -a` and `source .env` to allow your terminal to detect your AWS user

4. Now, agian run `cargo lambda watch` and `cargo lambda build` in the same time. Then run `cargo lambda deploy` to deploy your function to AWS. Go to Lambda in AWS, your function should show up there.
5. Go to API Gateway in AWS, create a new REST API with a new stage. Then create a `ANY` type method for your API. Then click on Deploy API, and you should see an invoke URL on the screen.

### Successful deployment of the `transform` function
This function has been successfully deployed to AWS Lambda in the following invoke URL: https://rt3v8c7a4k.execute-api.us-east-1.amazonaws.com/test/. The function being deployed to is under the name `tranform`. 
Below the picture indicating the success will be provided:
- AWS Lambda Function overview
![Function overview](https://gitlab.com/HathawayLiu/mini-project-2/-/raw/main/image/function-overview.png)
- Test successfully
![test](https://gitlab.com/HathawayLiu/mini-project-2/-/raw/main/image/test.png)