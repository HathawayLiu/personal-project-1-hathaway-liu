# Personal Project 1: Static Personal Website With Zola
## Project Description
In this project, we are asked to create a static website using Zola. For my specific website, I choose to clone the theme `DeepThoughts` that are provided in the official [Zola Website](https://www.getzola.org/themes/). I modify the original theme to contain `About` and `Project` pages in my personal website.

## Implementation
1. **Installation:** First, you need to install Zola. Visit the [official Zola documentation](https://www.getzola.org/documentation/getting-started/installation/) for detailed installation instructions for your operating system.
2. **Creating a New Site:** Once Zola is installed, you can create a new site by running the following command in your terminal:

    ```shell
    zola init my_site
    ```

    Replace `my_site` with the name of your project. This command creates a new directory with some basic configuration files and folders.
3. **Exploring the Directory Structure:** Zola's initial directory structure includes several key folders:

    - `content`: Where your markdown files will live.
    - `static`: For static files like images and CSS that don't change.
    - `templates`: Contains HTML templates for your site.
    - `themes`: Contains HTML themes for your site.

4. **Using themes:** If you don't want to create your own themes, there are plenty of themes that you could use in the Zola website. Navigate to the [Themes in Zola](https://www.getzola.org/themes/), and simply type the following command in your own terminal:
    ```shell
    cd themes
    git clone < theme repository URL >
    ``` 
    You can go under any theme post and find the corresponding repo to replace. Now, you can go to the theme directory and change the `Config.toml` with your own choice.

5. **Adding Content:** Create a new markdown file in the `content` directory to start adding content to your site. You can use the header format provided at the beginning of this document as a template for your content files. For my own website, I add the following contents:
    - `About`: A personal biographical page that contains my personal information, pictures and past experiences; styled with CSS under a `_index.md` file.
    - `Projects`: Mini Project 1 to 5 that I have done in IDS 721. Each project has its own folder and `index.md` file. The md files include the project description and implementation details for each project.
6. **Test and Build:** With Zola, you can first test your website by running `zola serve`. A url will appear in the terminal and you can get a preview of how your wesite looks. Then, you can build your site by running `zola build`. This command generates a public directory with your site's static files, ready to be deployed to a web server.
